import { AdviceCommand } from './commands/member/AdviceCommand'
import {
  IAppAccessors,
  IConfigurationExtend,
  IEnvironmentRead,
  IHttp,
  ILogger,
  IModify,
  IRead,
} from '@rocket.chat/apps-engine/definition/accessors'
import { App } from '@rocket.chat/apps-engine/definition/App'
import { IAppInfo } from '@rocket.chat/apps-engine/definition/metadata'
import { ISetting, SettingType } from '@rocket.chat/apps-engine/definition/settings'
import { BoopCommand } from './commands/member/BoopCommand'
import { settings } from './Settings'
import { IFileUploadContext, IPreFileUpload } from '@rocket.chat/apps-engine/definition/uploads'
import { FileUploadNotAllowedException } from '@rocket.chat/apps-engine/definition/exceptions'
import { IUser } from '@rocket.chat/apps-engine/definition/users'
import { IRoom } from '@rocket.chat/apps-engine/definition/rooms'
import { HeadpatCommand } from './commands/member/HeadpatCommand'
import { HugCommand } from './commands/member/HugCommand'
import { CatCommand } from './commands/member/CatCommand'

export class LilApp extends App implements IPreFileUpload {

  private uploadRole: string
  private disallowedUploadRooms: string[]

  constructor(info: IAppInfo, logger: ILogger, accessors: IAppAccessors) {
    super(info, logger, accessors)
  }

  public async onEnable (environment: IEnvironmentRead): Promise<boolean> {
    this.uploadRole = await environment.getSettings().getValueById(settings.required_upload_role)
    const disallowedUploadRooms: string = await environment.getSettings().getValueById(settings.disallowed_upload_rooms)
    this.disallowedUploadRooms = disallowedUploadRooms.split(',')
    return true
  }

  public async onSettingUpdated (setting: ISetting): Promise<void> {
    if (settings.required_upload_role === setting.id) {
      this.uploadRole = setting.value
    } else if (settings.disallowed_upload_rooms === setting.id) {
      this.disallowedUploadRooms = setting.value.split(',')
    }
  }

  /**
   * Checks if user has upload role. If no uploadRole defined, then returns false
   * @param user IUser
   * @returns boolean
   */
  private userHasUploadRole (user: IUser): boolean {
    if (this.uploadRole !== '' && user.roles.includes(this.uploadRole)) {
      return true
    }
    return false
  }

  /**
   * Checks if room is allowed for uploads
   * @param room IRoom
   * @returns boolean
   */
  private isRoomAllowed (room: IRoom): boolean {
    return !this.disallowedUploadRooms.includes(room.slugifiedName)
  }

  public async executePreFileUpload (context: IFileUploadContext, read: IRead, http: IHttp, IPersistance, modify: IModify): Promise<void> {
    const userID = context.file.userId
    const user = await read.getUserReader().getById(userID)

    const roomID = context.file.rid
    const room = await read.getRoomReader().getById(roomID)

    if (this.userHasUploadRole(user)) {
      return
    }

    if (room && this.isRoomAllowed(room)) {
      return
    }

    throw new FileUploadNotAllowedException("File uploads are not allowed in this room.")
  }

  protected async extendConfiguration (configuration: IConfigurationExtend): Promise<void> {
    // Settings
    await configuration.settings.provideSetting({
      id: settings.required_upload_role,
      type: SettingType.STRING,
      packageValue: '',
      required: false,
      public: false,
      i18nLabel: 'Required_Upload_Role_Label',
      i18nDescription: 'Required_Upload_Role_Description',
    })
    await configuration.settings.provideSetting({
      id: settings.disallowed_upload_rooms,
      type: SettingType.STRING,
      packageValue: '',
      required: false,
      public: false,
      i18nLabel: 'Disallowed_Upload_Rooms_Label',
      i18nDescription: 'Disallowed_Upload_Rooms_Description',
    })

    // Slash Commands
    await configuration.slashCommands.provideSlashCommand(new BoopCommand(this))
    await configuration.slashCommands.provideSlashCommand(new AdviceCommand(this))
    await configuration.slashCommands.provideSlashCommand(new HeadpatCommand(this))
    await configuration.slashCommands.provideSlashCommand(new HugCommand(this))
    await configuration.slashCommands.provideSlashCommand(new CatCommand(this))
  }
}
