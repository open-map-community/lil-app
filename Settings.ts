export enum settings {
  required_upload_role = "UPLOAD_ROLE",
  disallowed_upload_rooms = "DISALLOWED_UPLOAD_ROOMS"
}
