import { IHttp, IModify, IRead } from '@rocket.chat/apps-engine/definition/accessors'
import { App } from '@rocket.chat/apps-engine/definition/App'
import {
  ISlashCommand,
  SlashCommandContext,
} from '@rocket.chat/apps-engine/definition/slashcommands'

export class AdviceCommand implements ISlashCommand {
  public command = 'advice';
  public i18nDescription = 'advice_command_description';
  public i18nParamsExample = 'advice_command_example';
  public providesPreview = false;

  constructor(private readonly app: App) { }

  public async executor (context: SlashCommandContext, read: IRead, modify: IModify, http: IHttp): Promise<void> {

    const response = await http.get('https://api.adviceslip.com/advice')

    if (response.content) {
      const slipObject = JSON.parse(response.content).slip as ISlipObject
      const message = slipObject.advice
      const messageStructure = await modify.getCreator().startMessage()
      const room = context.getRoom()

      messageStructure
        .setRoom(room)
        .setText(message)

      await modify.getCreator().finish(messageStructure)
    }

  }
}

interface ISlipObject {
  slip_id: BigInt /* The unique ID of this advice slip. */
  advice: string /* The advice being given. */
}
interface ISearchObject {
  total_results: BigInt /* Total number of matching advice slips found. */
  query: BigInt /* The search query provided. */
  slips: string /* An array of slip objects matching the search query. */
}
interface IMessageObject {
  type: string /* The type of messages. Can be either `noticed`, `warning` or `error`. */
  text: string /* The messages being received. */
}
