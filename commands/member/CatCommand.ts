import { IHttp, IModify, IPersistence, IRead } from '@rocket.chat/apps-engine/definition/accessors'
import { App } from '@rocket.chat/apps-engine/definition/App'
import { IMessageAttachment } from '@rocket.chat/apps-engine/definition/messages'
import {
  ISlashCommand,
  ISlashCommandPreview,
  ISlashCommandPreviewItem,
  SlashCommandContext,
  SlashCommandPreviewItemType
} from '@rocket.chat/apps-engine/definition/slashcommands'

class ImageAttachment implements IMessageAttachment {
  imageUrl?: string

  constructor(imgUrl: string) {
    this.imageUrl = imgUrl
  }
}

class ImagePreviewItem implements ISlashCommandPreviewItem {
  id: string
  type: SlashCommandPreviewItemType = SlashCommandPreviewItemType.IMAGE
  value: string
  text: string

  constructor(id: string, value: string, text: string) {
    this.id = id
    this.value = value
    this.text = text
  }
}

class CommandPreview implements ISlashCommandPreview {
  i18nTitle: string
  items: ImagePreviewItem[]

  constructor(i18nTitle: string, items: ImagePreviewItem[]) {
    this.i18nTitle = i18nTitle
    this.items = items
  }
}

export class CatCommand implements ISlashCommand {
  public command = 'cat';
  public i18nDescription = 'cat_command_description';
  public i18nParamsExample = 'cat_command_example';
  public providesPreview = true;

  constructor(private readonly app: App) { }

  public async previewer (context: SlashCommandContext, read: IRead, modify: IModify, http: IHttp, persis: IPersistence): Promise<ISlashCommandPreview> {
    let items: ImagePreviewItem[] = []
    const args = context.getArguments()
    if (args) {
      const selected_breed_json = await http.get('https://api.thecatapi.com/v1/breeds/search?q=' + args.join(" "), {
        headers: { Accept: 'application/json' }
      })
      if (selected_breed_json.statusCode == 200 && selected_breed_json.content) {
        const content = JSON.parse(selected_breed_json.content) as IBreedSearch
        const selected_breed = content[0]
        const breed_id = selected_breed.id

        const cats_json = await http.get('https://api.thecatapi.com/v1/images/search?size=thumb&limit=10&order=RANDOM&breed_ids=' + breed_id, {
          headers: { Accept: 'application/json' }
        })
        if (cats_json.statusCode == 200 && cats_json.content) {
          const content = JSON.parse(cats_json.content) as IImagesSearch
          const cats = content
          items = cats.map(cat => new ImagePreviewItem(cat.id, cat.url, selected_breed.description))
        }
      }
    }
    return new CommandPreview("Cats", items)
  }

  public async executePreviewItem (item: ImagePreviewItem, context: SlashCommandContext, read: IRead, modify: IModify, http: IHttp, persis: IPersistence): Promise<void> {
    const messageStructure = await modify.getCreator().startMessage()
    const sender = context.getSender()
    const room = context.getRoom()

    const attachment: IMessageAttachment = new ImageAttachment(item.value)

    messageStructure
      .setRoom(room)
      .setSender(sender)
      .setText(item.text)
      .addAttachment(attachment)

    await modify.getCreator().finish(messageStructure)
  }

  public async executor (context: SlashCommandContext, read: IRead, modify: IModify, http: IHttp): Promise<void> {
    const response = await http.get('https://api.thecatapi.com/v1/images/search', {
      headers: { Accept: 'application/json' }
    })

    if (response.statusCode == 200 && response.content) {
      const content = JSON.parse(response.content) as IImagesSearch
      const cat = content[0]
      const messageStructure = await modify.getCreator().startMessage()
      const sender = context.getSender()
      const room = context.getRoom()

      const attachment: IMessageAttachment = new ImageAttachment(cat.url)

      messageStructure
        .setRoom(room)
        .setSender(sender)
        .addAttachment(attachment)

      await modify.getCreator().finish(messageStructure)
    }
  }
}

interface ICatBreed {
  id: string,
  name: string,
  temperament: string,
  life_span: string,
  alt_names: string,
  description: string,
  wikipedia_url: string,
  cfa_url: string,
  origin: string,
  weight_imperial: string,
  experimental: 0 | 1,
  hairless: 0 | 1,
  natural: 0 | 1,
  rare: 0 | 1,
  rex: 0 | 1,
  suppress_tail: 0 | 1,
  short_legs: 0 | 1,
  hypoallergenic: 0 | 1,
  adaptability: 1 | 2 | 3 | 4 | 5,
  affection_level: 1 | 2 | 3 | 4 | 5,
  country_code: string,
  country_codes: string,
  child_friendly: 1 | 2 | 3 | 4 | 5,
  dog_friendly: 1 | 2 | 3 | 4 | 5,
  energy_level: 1 | 2 | 3 | 4 | 5,
  grooming: 1 | 2 | 3 | 4 | 5,
  health_issues: 1 | 2 | 3 | 4 | 5,
  intelligence: 1 | 2 | 3 | 4 | 5,
  shedding_level: 1 | 2 | 3 | 4 | 5,
  social_needs: 1 | 2 | 3 | 4 | 5,
  stranger_friendly: 1 | 2 | 3 | 4 | 5,
  vocalisation: 1 | 2 | 3 | 4 | 5,
}

interface ICategory {
  id: number,
  name: string
}

interface IBreedSearch extends Array<any> {
  [index: number]: ICatBreed
}

interface IImagesSearch extends Array<any> {
  [index: number]: {
    id: string,
    url: string,
    sub_id: string,
    created_at: string,
    original_filename: string,
    categories: {
      [index: number]: ICategory
    }
    breeds: {
      [index: number]: ICatBreed
    }
  }
}
