import { ISlashCommand, SlashCommandContext } from '@rocket.chat/apps-engine/definition/slashcommands'
import { IModify, IRead } from '@rocket.chat/apps-engine/definition/accessors'
import { App } from '@rocket.chat/apps-engine/definition/App'

export class HeadpatCommand implements ISlashCommand {
  public command = 'headpat';
  public i18nDescription = 'headpat_command_description';
  public i18nParamsExample = 'headpat_command_example';
  public providesPreview = false;

  constructor(private readonly app: App) { }

  public async executor (context: SlashCommandContext, read: IRead, modify: IModify): Promise<void> {
    const args = context.getArguments()
    const message = `_Headpats ${args[0]}!_ :cat_headpat:`

    const messageStructure = await modify.getCreator().startMessage()
    const sender = context.getSender() // the user calling the slashcommand
    const room = context.getRoom() // the current room

    messageStructure
      .setSender(sender)
      .setRoom(room)
      .setText(message)

    await modify.getCreator().finish(messageStructure)
  }
}
